package com.vp.common

import com.bumptech.glide.annotation.GlideModule

@GlideModule
class AppGlideModule : com.bumptech.glide.module.AppGlideModule()

package com.vp.favorites.viewmodel

enum class FavoriteListState {
    IN_PROGRESS, LOADED, ERROR
}
